package main

// InMemoryDatabase defines the interface for an in-memory database.
type InMemoryDatabase interface {
	// Get retrieves the value associated with the given key.
	//
	// Parameters:
	//   key (string): The key to retrieve.
	//
	// Returns:
	//   interface{}: The value associated with the key or nil if the key does not exist.
	Get(key string) interface{}

	// Set stores a key-value pair in the database.
	//
	// Parameters:
	//   key (string): The key to store.
	//   value (interface{}): The value to associate with the key.
	Set(key string, value interface{})

	// Delete removes the key-value pair associated with the given key.
	//
	// Parameters:
	//   key (string): The key to delete.
	Delete(key string)

	// StartTransaction starts a new transaction. All operations within this
	// transaction are isolated from others.
	StartTransaction()

	// Commit commits all changes made within the current transaction to the database.
	//
	// Returns:
	//   None
	Commit()

	// Rollback rolls back all changes made within the current transaction and discards them.
	//
	// Returns:
	//   None
	Rollback()
}
