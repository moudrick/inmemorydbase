# inmemorydbase

[Task](https://docs.google.com/document/d/1VNuK6HatwxI8nvvTbHBL_IYniAJmRqizYBU8qDFQk0Q)

## Explanation for choosing a certain approach. 

* As I am allowed to assume there is no multithread access, I decided to follow
  * KISS - keep it short and straightforward
  * Stack of maps indexed with string is selected as a storage carrier for im-memory database
  * Stack implemented with an array as no standard stack implementation in Golang Standard library. No wrapper class introduced to keep implementation detaild shorter and more straightforward
* The examples to refer made me take advantage of applying TDD approach
  * TDD - test driven development
* Extracting interface from core functionality would allow to rapidly 
  * append other implementations if requirements change
  * quickly integrate and switch these implementations using DI
    * DI - dependency injection
* Comments placeholder left to insert assett-like code lines once unexpected error behavios clearly defined

## It works

```
git clone git@gitlab.com:moudrick/inmemorydbase.git
cd inmemorydbase

go test
```

```
PASS
ok      inmemorydbase   0.002s
```

Test results from vscode:

```
Running tool: /usr/local/go/bin/go test -timeout 30s -run ^(TestSimpleDB_Commit|TestSimpleDB_Rollback|TestSimpleDB_NestedTransactions|TestSimpleDB_NestedTransactionsWithRollback)$ inmemorydbase

=== RUN   TestSimpleDB_Commit
--- PASS: TestSimpleDB_Commit (0.00s)
=== RUN   TestSimpleDB_Rollback
--- PASS: TestSimpleDB_Rollback (0.00s)
=== RUN   TestSimpleDB_NestedTransactions
--- PASS: TestSimpleDB_NestedTransactions (0.00s)
=== RUN   TestSimpleDB_NestedTransactionsWithRollback
--- PASS: TestSimpleDB_NestedTransactionsWithRollback (0.00s)
PASS
ok      inmemorydbase   0.003s
```
