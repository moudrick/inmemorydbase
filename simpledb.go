package main

type txscope = map[string]interface{}

type simpleDB struct {
	txstack []map[string]interface{}
}

// NewSimpleDB creates a new instance of SimpleDB.
func NewSimpleDB() InMemoryDatabase {
	return &simpleDB{
		txstack: []txscope{make(txscope)},
	}
}

func (db *simpleDB) activeTxScope() txscope {
	// assert-like: len(db.txstack) > 0
	return db.txstack[len(db.txstack)-1]
}

func (db *simpleDB) Get(key string) interface{} {
	return db.activeTxScope()[key]
}

func (db *simpleDB) Set(key string, value interface{}) {
	db.activeTxScope()[key] = value
}

func (db *simpleDB) Delete(key string) {
	delete(db.activeTxScope(), key)
}

func (db *simpleDB) StartTransaction() {
	tx := make(txscope, len(db.activeTxScope()))
	for key, value := range db.activeTxScope() {
		tx[key] = value
	}
	db.txstack = append(db.txstack, tx)
}

func (db *simpleDB) Commit() {
	// Does not check if any transacton open before commit. Should it?
	// assert-like: len(db.txstack) > 1
	tx := db.txstack[len(db.txstack)-1]
	db.txstack = db.txstack[:len(db.txstack)-1]
	// assert-like: len(db.txstack) > 0
	db.txstack[len(db.txstack)-1] = tx
}

func (db *simpleDB) Rollback() {
	// Does not check if any transacton open before commit. Should it?
	db.txstack = db.txstack[:len(db.txstack)-1]
}
