package main

import "testing"

func TestSimpleDB_Commit(t *testing.T) {
	db := NewSimpleDB()
	db.Set("key1", "value1")
	db.StartTransaction()
	db.Set("key1", "value2")
	db.Commit()
	expect(t, db.Get("key1"), "value2", "Commit scenario failed")
}

func TestSimpleDB_Rollback(t *testing.T) {
	db := NewSimpleDB()
	db.Set("key1", "value1")
	db.StartTransaction()
	expect(t, db.Get("key1"), "value1", "Rollback scenario failed at start")
	db.Set("key1", "value2")
	expect(t, db.Get("key1"), "value2", "Rollback scenario failed before rollback")
	db.Rollback()

	expect(t, db.Get("key1"), "value1", "Rollback scenario failed")
}

func TestSimpleDB_NestedTransactions(t *testing.T) {
	db := NewSimpleDB()
	db.Set("key1", "value1")
	db.StartTransaction()
	db.Set("key1", "value2")
	expect(t, db.Get("key1"), "value2", "Nested scenario failed before nested start")

	db.StartTransaction()
	expect(t, db.Get("key1"), "value2", "Nested scenario failed after nested start")

	db.Delete("key1")
	db.Commit()
	expect(t, db.Get("key1"), nil, "Nested scenario failed after nested commit")

	db.Commit()
	expect(t, db.Get("key1"), nil, "Nested scenario failed after outer commit")
}

func TestSimpleDB_NestedTransactionsWithRollback(t *testing.T) {
	db := NewSimpleDB()
	db.Set("key1", "value1")
	db.StartTransaction()
	db.Set("key1", "value2")
	expect(t, db.Get("key1"), "value2", "Nested rollback scenario failed before nested start")

	db.StartTransaction()
	expect(t, db.Get("key1"), "value2", "Nested rollback scenario failed before nested start")

	db.Delete("key1")
	db.Rollback()
	expect(t, db.Get("key1"), "value2", "Nested rollback scenario failed after nested rollback")

	db.Commit()
	expect(t, db.Get("key1"), "value2", "Nested rollback scenario failed after outer commit")
}
