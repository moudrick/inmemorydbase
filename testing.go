package main

import ("runtime/debug";"testing")

func expect(t *testing.T, result interface{}, expected interface{}, errorf string) {
	if result != expected {
		t.Errorf("%s. Expected %v, got %v", errorf, expected, result)
		debug.PrintStack()
	}
}
